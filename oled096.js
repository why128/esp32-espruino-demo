/**
* 第一步链接WiFi
* 其他的分为 第二步  main 方法
* 1.创建http 服务
* 2.设置 esp32 芯片系统时间
* 3.获取高德api的天气接口数据  getweather 方法
* 4.调用小屏幕，把数据显示出来  start 方法
**/
//require("Wifi").scan(function(msg){console.log(msg);});
//ESP32.reboot();
let wifi = require("Wifi");
let DateExt = require('DateExt.min');
let sntp = require('sntp.min');
var options = {
    host: 'ntp1.aliyun.com',  // Defaults to pool.ntp.org
    port: 123,                      // Defaults to 123 (NTP)
    resolveReference: true,         // Default to false (not resolving)
    timeout: 5000                   // Defaults to zero (no timeout)
};

let now = new Date();
let ssid = "XXXXXX";  // Your wifi ssid
var password = "XXXXXXX"; // Your wifi password
var port = 8084;
let tqObject =  null;
var g = null;
let serverTips = '';
let runtime = 0;
let t = null;
let timeobj = {};
function main(){
  now.setTime(parseInt(timeobj.T2) + 8 * 60 * 60 * 1000);
  //let nows = Date(parseInt(timeobj.T2) + 8 *60 *60 *1000).as("UY-0M-0D 0h:0m:0s'Z").str;
  // 1.....
  var http = require('http');
  let processRequest = function(req, res){
    res.writeHead(200);
    res.end(`
      <div style='display:flex;justify-content: center;align-items: center;flex-direction: column;height: 100vh;'>
      <h2 style='text-align:center;'>This is eps32's server!</h2>
      <p style='text-align:center;'>now wifi name is: ${ssid}</p>
      <p style='text-align:center;'>now wifi status: ${JSON.stringify(wifi.getStatus())}</p>
      <p style='text-align:center;'>server's time is: ${Date(sntp.now()+ 8 * 60 *60 * 1000).as("UY-0M-0D 0h:0m:0s'Z").str}</p>
      </div>
    `);
  };
  http.createServer(processRequest).listen(port);
  serverTips = `${wifi.getIP().ip}:${port}`;
  console.log(`Web server running at http://${serverTips}`);
  // 2.....
  //DateExt.locale({zone:['UTC','UTC'], offset:[-8,480], dst:0, str:"UY-0M-0D'T'0h:0m:0s'Z'"});
  console.log(now,sntp.now());
  // 3......  4.....
  getweather(http, start);

}

function start(){
  let run = function () {setInterval(timerevent, 20,g);};
  // I2C
  I2C1.setup({scl:D15,sda:D4});
  g = require("SSD1306.min").connect(I2C1, run);
}

function getweather(http, callback){
  let url = `https://restapi.amap.com/v3/weather/weatherInfo?key=【你的_API_KEY】&city=420115&extensions=all`;
  let host = `restapi.amap.com`;
  let path = `/v3/weather/weatherInfo?key=e78bbfd6532c4cfcc37868fd8005b630&city=420115&extensions=all`;
  var options = {
    host: host, // host name
    port: 80,            // (optional) port, defaults to 80
    path: path,           // path sent to server
    method: 'GET',       // HTTP command sent to server (must be uppercase 'GET', 'POST', etc)
    protocol: 'http:',   // optional protocol - https: or http:
    headers: { } // (optional) HTTP headers
  };
  let datas = '';
  let req = http.request(options,function(res) {
  res.on('data', function(data) {
    datas += data;
  });
  res.on('close', function(data) {console.log("Connection closed",data);});
  res.on('end', function(data) {
    datas = JSON.parse(datas);
    if(datas && datas.status == 1){
      tqObject = datas.forecasts[0];
      // running
      callback();
    }
  });
  },function(err){console.log('err',err);});
  req.end();
}

function timerevent(g){
  // 计时器 时间+++
  runtime++;
  // 清除屏幕
  g.clear();
  //设置字体
  g.setFont("Vector",12);
  //header
  /*var img1 = Graphics.createImage(`
XXXXXXXXX
X       X
X   X   X
X   X   X
X       X
XXXXXXXXX
`);
  g.drawImage(img1, 3,4);*/
  g.drawString(Date(sntp.now() + 8 * 60 *60 * 1000).as("UY-0M-0D 0h:0m:0s'Z").str,0,4);
  // body
  //console.log('weather',tqObject);
  g.setFont("Vector",36);
  g.drawString(tqObject.casts[0].daytemp , 32, 16);
  g.drawCircle(80, 20, 3);
  g.drawString("c" , 82, 16);
  // 提示服务所在的端口 和 ip
  g.setFont("Vector",12);
  g.drawString(`${serverTips}`, 2, 48);
  // loading 动画
  for(let i = 0; i < runtime; i++){
    g.drawRect(4*i,0, 4*i+4, 2);
    g.drawRect(4*i,61, 4*i+4, 63);
  }
  if(runtime > 31){
    runtime = 0;
  }
  g.flip();
}

// 运行代码
if(!wifi.getStatus() || !wifi.getStatus().ssid){
  wifi.connect(ssid, {password: password}, function(err) {
    if(!err){
      wifi.save(); // Next reboot will auto-connect
      sntp.time(options,function(err,result){
        if(!err){
          console.log('timer666',result);
          timeobj = result;
          sntp.start({},main);
        }else{
          console.log('timer err',err);
        }
      });
    }else{
      console.log(err);
    }
  });
}else{ // Next reboot will auto-connect
  sntp.time(options,function(err,result){
    if(!err){
      console.log('timer666',result);
      timeobj = result;
      sntp.start({},main);
    }else{
    console.log('timer err',err);
    }
  });
}
var on = false;
setInterval(function() {
  on = !on;
  digitalWrite(Pin(2),on);
}, 500);
console.log('last line ``````````` ');